"use strict";
require("./config/read_env");
var express = require("express");
var app = express();

var logger = require("./config/logger");
var routes = require("./routes");
var path = require("path");

// var Promise = require('bluebird');
var bodyParser = require("body-parser");

app.use(express.static("./dist"));

var bookListingTemplate = require("../templates/book/book-listing.handlebars");

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

logger.info(path.join(__dirname, "app/views"));

app.use(function(req, res, next) {
  logger.info(req.method + "" + req.originalUrl);
  next();
});

app.use("/", routes);

app.listen(process.env.PORT, () =>
  logger.info(`server is running at the port ${process.env.PORT}`)
);
